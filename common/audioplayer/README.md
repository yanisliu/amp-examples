### audioplayer示例

* 说明
audioplayer是IoT轻应用内置的音频播放组件。
支持mp3、wav格式音频。
支持本地和http音频文件播放。
支持播放/暂停/继续/停止/定位播放等操作。

* 使用
参考audioplayer示例及API说明文档