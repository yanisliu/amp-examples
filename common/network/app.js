var network = require('network');

var net = network.openNetWorkClient();

/** 获取网络类型
* 目前支持两种类型：wifi cellular（蜂窝网）
*/
var type = net.getType();
console.log('net type is: ' + type);

/** 获取网络状态
* 目前支持两种状态：connect disconnect
*/
var status = net.getStatus();
console.log('net status is: ' + status);

net.on('connect', function() {
    console.log('net connect success');
});

net.connect({
    ssid: 'ssid',
    password: 'password'
});