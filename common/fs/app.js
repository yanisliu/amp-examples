var fs = require('fs');

var path = './test.data';
var content = 'this is amp fs test file';

// write file
fs.writeSync(path, content);

// read file
var data = fs.readSync(path);
console.log('fs read: ' + data);

// delete file
fs.unlinkSync(path);