/*
 * @Author: your name
 * @Date: 2020-07-29 17:11:52
 * @LastEditTime: 2020-09-24 10:37:10
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /amp-ec100/iot/app.js
 */
console.log('iot: testing iot...');
var iot = require('iot');
var gpio = require('gpio');
var i2c = require('i2c');
var network = require('network');

var net = network.openNetWorkClient();

/* 设备三元组信息获取请参考common目录下的iot组件说明 */
var productKey = 'your productKey'; /* your productKey */
var deviceName = 'your deviceName'; /* your deviceName */
var deviceSecret = 'your deviceSecret'; /* your deviceSecret */

var lm75 = i2c.open({
    id: 'I2C0',
    success: function () {
        console.log('bmp280 sensor open success')
    },
    fail: function () {
        console.log('bmp280 sensor open failed')
    }
});

var led1 = gpio.open({
    id: 'D1',
});
led1.writeValue(1);

var led2 = gpio.open({
    id: 'D2',
});
led1.writeValue(1);

var led3 = gpio.open({
    id: 'D3',
});
led3.writeValue(1);

var led4 = gpio.open({
    id: 'D4',
});
led4.writeValue(1);

var beep = gpio.open({
    id: 'D5',
    success: function () {
        console.log('gpio: open beep success')
    },
    fail: function () {
        console.log('gpio: open beep failed')
    }
});
beep.writeValue(1);

// 构建物联网连接实例
var device;
net.on('connect', function () {
    device = iot.device({
        productKey: productKey,
        deviceName: deviceName,
        deviceSecret: deviceSecret,
        region: 'cn-shanghai',
        success: function () {
            console.log('iot: [success] connect');
            // onConnect();
        },
        fail: function () {
            console.log('iot: [failed] connect');
        }
    });
})

// 获取传感器数据
function lm75tmpGet() {
    var temp;
    var sig = 1;

    var regval = lm75.readMem(0x00, 2); // 读传感器数据
    // 温度数据转换
    var tempAll = (regval[0] << 8) + regval[1];
    console.log('tempAll is ' + tempAll);

    if (regval[0] & 0x80 != 0) {
        tempAll = ~(tempAll) + 1;
        sig = -1;
    }
    tempAll = tempAll >> 5;
    console.log('tempAll final data ' + tempAll);
    temp = tempAll * 0.125 * sig;

    return temp;
}

var postCnt = 10;
var lightSwitch = 0;

// 业务逻辑
// 3秒采集并上传一次温度数据
setInterval(function () {
    temp = lm75tmpGet();
    device.postProps({
        payload: {
            IndoorTemperature: temp
        }
    })
    console.log('lm75 data is ' + temp);
}, 3000);

device.on('connect', function () {
    console.log('iot: [success] iot.on(\'connect\')');
    led2.writeValue(0);
});

/* 网络断开事件 */
device.on('disconnect', function () {
    console.log('iot: [success] iot.on(\'disconnect\')');
    led2.writeValue(1);
});

/* 关闭连接事件 */
device.on('close', function () {
    console.log('iot: [success] iot.on(\'close\')');
});

/* 发生错误事件 */
device.on('error', function (err) {
    throw new Error('iot: [failed] iot.on(\'error\') ' + err);
});

/* 云端设置属性事件 */
device.on('props', function (payload) {
    console.log('iot: [success] iot.on(\'props\'), payload: ' + JSON.stringify(payload));
    if (payload["LightSwitch"] == '1') {
        led1.writeValue(1);
        device.postProps({
            payload: {
                LightSwitch: 1
            }
        })
    }
    if (payload["LightSwitch"] == '0') {
        led1.writeValue(0);
        device.postProps({
            payload: {
                LightSwitch: 0
            }
        })
    }
});

/* 云端下发服务事件 */
device.on('service', function (id, payload) {
    console.log('iot: [success] iot.on(\'service\'), id: ' + id + ', payload: ' + JSON.stringify(payload));
});