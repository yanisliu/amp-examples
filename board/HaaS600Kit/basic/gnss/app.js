var uart = require('uart');

function ArrayToString(fileData) {
  var dataString = "";
  for (var i = 0; i < fileData.length; i++) {
    dataString += String.fromCharCode(fileData[i]);
  }
  return dataString;
}

// gnss uart
var gnss = uart.open({
  id: 'UART1'
});

// gnss uart data receive 
gnss.on('data', function(data) {
  console.log(ArrayToString(data));
});