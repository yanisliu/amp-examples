var spi = require('spi');

var msgbuf = [0x01, 0x02, 0x10, 0xaa];
// spi sensor
var sensor = spi.open({
  id: 'SPI0'
});

setInterval(function () {

  var value = sensor.readWrite(msgbuf, 4);

  console.log('sensor value is ' + value);
}, 1000);
