var gpio = require('gpio');

var led = gpio.open({
	id: 'D13'
});

var vol = 0;

setInterval(function () {
	vol = 1 - vol;

	led.writeValue(vol);
	console.log('gpio: led set value ' + vol);

	vol = led.readValue();
	console.log('gpio: led get value ' + vol);
}, 1000);

