### LED 点灯示例
* 配置
    ```
    "io": {
        "D13": {
            "type": "GPIO",     /* 外设使用GPIO */
            "port": 33,         /* GPIO33引脚  */
            "dir": "output",    /* GPIO33引脚配置为输出模式 */
            "pull": "pulldown"  /* GPIO配置为下拉模式 */
        }
    }

    ```

* 应用

    1、通过id打开GPIO
    2、通过GPIO API控制输出高低电平开关灯，每隔一秒钟反转一次并打印出GPIO电平值