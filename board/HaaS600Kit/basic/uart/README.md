### UART示例
* 配置
    ```
    "io": {
      "UART1":{
        "type":"UART",           /* 外设类型为UART */
        "port":1,                /* UART端口号     */
        "dataWidth":8,           /* 串口数据宽度值 */
        "baudRate":115200,       /* 串口波特率     */
        "stopBits":1,            /* 串口停止位     */
        "flowControl":"disable", /* 流控设置       */
        "parity":"none"          /* 奇偶校验设置   */
      }
    }

    ```

* 应用

    1、创建SPI实例，port为1，对应的HaaS600上的管脚为IO53和IO54；
    2、通过PC端串口工具连接；
	3、运行用例，可以看到设备端发送的数据；
	4. 串口工具下发数据，设备端接收后通过字符串形式输出；