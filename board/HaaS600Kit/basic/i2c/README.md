### I2C示例
* 配置
    ```
    "io": {
      "I2C0": {
        "type": "I2C",      /* 外设通过I2C连接 */
        "port": 0,          /* I2C port口     */
        "addrWidth": 7,     /* 地址宽度       */
        "freq": 400000,     /* I2C总线频率    */
        "mode": "master",   /* I2C总线模式    */
        "devAddr": 72       /* I2C从设备地址  */
      }
    }

    ```

* 应用

    1、创建I2C实例，port 0对应的I2C已固定连接了lm75温度传感器；
    2、每隔1秒钟，通过I2C读取一次温度；